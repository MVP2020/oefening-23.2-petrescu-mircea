﻿using System;
using System.Windows;

namespace _23._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public string BerekenenOmtrek()
        {
            int straal = Convert.ToInt32(input.Text);
            double pi = 3.14;
            double per_cir;
            per_cir = 2 * pi * straal;
            return per_cir.ToString();
        }
        public string BerekenenOppervlakte()
        {
            int straal = Convert.ToInt32(input.Text);
            double pi = 3.14;
            double area_circle;
            area_circle = pi * (straal * straal);
            return area_circle.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int straal = Convert.ToInt32(input.Text);
            Straalis.Content = straal;
            Omtrek.Content = BerekenenOmtrek().ToString();
            Oppervlakte.Content = BerekenenOppervlakte().ToString();
        }
    }
}
